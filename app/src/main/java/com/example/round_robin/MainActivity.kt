package com.example.round_robin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.SparseBooleanArray
import android.view.View
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent
import android.widget.Button


//const val EXTRA_MESSAGE = "com.example.round_robin.MESSAGE"

class MainActivity : AppCompatActivity() {
    companion object {
        var itemList = arrayListOf<String>()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val beginBtn: Button = findViewById(R.id.begin)
        beginBtn.setAlpha(.5f);
        beginBtn.setClickable(false);
        fun beginStatus(){
            if (itemList.size >= 2) {
                beginBtn.setAlpha(1f);
                beginBtn.setClickable(true);
            }
            else if (itemList.size <2) {
                beginBtn.setAlpha(.5f);
                beginBtn.setClickable(false);
            }
        }
        beginStatus()

        supportActionBar?.setTitle("Round Robin")

        // Initializing the array lists and the adapter
        var adapter = ArrayAdapter<String>(this, R.layout.simple_multiple_choice, itemList)
        listView.setAdapter(adapter)
        adapter.notifyDataSetChanged()
        add.setOnClickListener {
            itemList.add(editText.text.toString())
//            listView.adapter = adapter
            adapter.notifyDataSetChanged()
            // This is because every time when you add the item the input      space or the eidt text space will be cleared
            editText.text.clear()
            beginStatus()
        }
        // Selecting and Deleting the items from the list when the delete button is pressed
        delete.setOnClickListener {
            val position: SparseBooleanArray = listView.checkedItemPositions
            val count = listView.count
            var item = count - 1
            while (item >= 0) {
                if (position.get(item))
                {
                    adapter.remove(itemList.get(item))
                }
                item--
            }
            position.clear()
            adapter.notifyDataSetChanged()
            beginStatus()
        }
        // Clearing all the items in the list when the clear button is pressed
        clear.setOnClickListener {

            itemList.clear()
            adapter.notifyDataSetChanged()
            beginStatus()

        }
        // Adding the toast message to the list when an item on the list is pressed
        listView.setOnItemClickListener { adapterView, view, i, l ->
            android.widget.Toast.makeText(this, "You Selected the item --> "+itemList.get(i), android.widget.Toast.LENGTH_SHORT).show()
        }

    }
    fun beginBattle(view: View) {
        val intent = Intent(this, BattleActivity::class.java)
        startActivity(intent)
    }
}