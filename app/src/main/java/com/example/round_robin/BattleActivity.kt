package com.example.round_robin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlin.random.Random
import kotlinx.android.synthetic.main.activity_battle.*
import kotlinx.android.synthetic.main.activity_main.*

class BattleActivity : AppCompatActivity() {
    companion object {
        var resultsList = arrayListOf<String>()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_battle)
        supportActionBar?.setTitle("Round Robin")
        //set back button
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val matches =  matchups().shuffled()
        var rc = 1
        val button1: Button = findViewById(R.id.entrantBtn1)
        button1.text = matches.elementAt(0).first
        val button2: Button = findViewById(R.id.entrantBtn2)
        button2.text = matches.elementAt(0).second
        val button3: Button = findViewById(R.id.resultsBtn)
        button3.text = "See Results"
        val matchupText: TextView = findViewById(R.id.matchupText)
        matchupText.text = "Match ${rc}"
        resultsList.clear()
        // Results and Button Updates
        entrantBtn1.setOnClickListener {
            if (rc < matches.size) {
                resultsList.add(button1.text as String)
                button1.text = matches.elementAt(rc).first
                button2.text = matches.elementAt(rc).second
                rc += 1
                matchupText.text = "Match ${rc}"
                println(resultsList)
            }
            else if (rc >= matches.size) {
                resultsList.add(button1.text as String)
                println(resultsList)
                button1.text = "Check Results"
                button2.text = "Check Results"
                button1.visibility = View.GONE
                button2.visibility = View.GONE
                button3.visibility = View.VISIBLE
                matchupText.text = "Matches Complete!"
            }
        }
        entrantBtn2.setOnClickListener {
            if (rc < matches.size) {
                resultsList.add(button2.text as String)
                button1.text = matches.elementAt(rc).first
                button2.text = matches.elementAt(rc).second
                rc += 1
                matchupText.text = "Match ${rc}"
                println(resultsList)
            }
            else if (rc >= matches.size) {
                resultsList.add(button2.text as String)
                println(resultsList)
                button1.text = "Check Results"
                button2.text = "Check Results"
                button1.visibility = View.GONE
                button2.visibility = View.GONE
                button3.visibility = View.VISIBLE
                matchupText.text = "Matches Complete!"
            }
        }
    }
    fun matchups(): ArrayList<Pair<String, String>> {
        var items = MainActivity.Companion.itemList
        var i = 0
        var matchupsList = arrayListOf<Pair<String,String>>()
        for (element in items) {
            for ((index, item) in items.withIndex()) {
                if (item != items.elementAt(i) && index > i) {
                    matchupsList.add(
                        Pair(
                            items.elementAt(i),
                            item
                        )
                    )
                }
            }
            if (element != items.last()) {
                    i+=1
            }
        }
        println(matchupsList)
        return matchupsList
    }
    fun showResults(view: View) {
        val intent = Intent(this, Results::class.java)
        startActivity(intent)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}