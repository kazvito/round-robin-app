package com.example.round_robin

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.view.Gravity
import android.view.View
import kotlinx.android.synthetic.main.activity_battle.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.*

import android.graphics.Typeface
import android.widget.*


class Results : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_results)
        supportActionBar?.setTitle("Final Results")
        //set back button
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val results = countVotes()

    }

    @SuppressLint("SetTextI18n")
    fun countVotes(): Map<String, Int> {
        var results = BattleActivity.resultsList
        var frequencies = results.groupingBy{ it }.eachCount()
        println(frequencies)
        var sorted = frequencies.toList().sortedBy { (_, value) -> value}.toMap()
        var sortedKeys = (sorted.keys).toTypedArray().reversed()
        var sortedValues = (sorted.values).toTypedArray().reversed()


        val layout = findViewById<TableLayout>(R.id.resultsTable)

        for ((index,value) in sortedValues.withIndex()) {
            //Create Row
            val row = TableRow(this)
            layout.addView(row)

            //Create Columns
            val leftColumn = TextView(this)
            val rightColumn = TextView(this)

            var score = value
            var scoreString = score.toString()
            var totalMatches = MainActivity.itemList.size - 1
            var winLoss = (totalMatches - score).toString()

            leftColumn.gravity = Gravity.LEFT
            leftColumn.setPadding(2, 2, 2, 2)
            leftColumn.text = sortedKeys.elementAt(index)
            leftColumn.setTextSize(34F)
            rightColumn.gravity = Gravity.RIGHT
            rightColumn.setPadding(2, 2, 2, 2)
            rightColumn.text = scoreString + "-" + winLoss
            rightColumn.setTextSize(34F)
            row.addView(leftColumn)
            row.addView(rightColumn)
        }
    var losers = MainActivity.itemList.minus(sortedKeys)
        for ((index,value) in losers.withIndex()) {
            //Create Row
            val row = TableRow(this)
            layout.addView(row)

            //Create Columns
            val leftColumn = TextView(this)
            val rightColumn = TextView(this)

            var score = 0
            var scoreString = score.toString()
            var totalMatches = MainActivity.itemList.size - 1
            var winLoss = (totalMatches - score).toString()

            leftColumn.gravity = Gravity.LEFT
            leftColumn.setPadding(2, 2, 2, 2)
            leftColumn.text = value
            leftColumn.setTextSize(34F)
            rightColumn.gravity = Gravity.RIGHT
            rightColumn.setPadding(2, 2, 2, 2)
            rightColumn.text = scoreString + "-" + winLoss
            rightColumn.setTextSize(34F)
            row.addView(leftColumn)
            row.addView(rightColumn)
        }


        return frequencies
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}